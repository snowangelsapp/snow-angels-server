.. Snow Angels documentation master file, created by
   sphinx-quickstart on Thu Apr 14 01:16:30 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Snow Angels's documentation!
=======================================

Contents:
---------

.. toctree::
   :maxdepth: 2

   api_doc
   src_doc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

handlers package
================

Module contents
---------------

.. automodule:: handlers
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

handlers.alerts module
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: handlers.help_request
    :members:
    :undoc-members:
    :show-inheritance:

handlers.database module
~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: handlers.database
    :members:
    :undoc-members:
    :show-inheritance:

handlers.google module
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: handlers.google
    :members:
    :undoc-members:
    :show-inheritance:

handlers.pages module
~~~~~~~~~~~~~~~~~~~~~

.. automodule:: handlers.pages
    :members:
    :undoc-members:
    :show-inheritance:



Snow Angels API Documentation
=============================

url: http://snow-angels-server.appspot.com/

**Table of Contents**

* `Routes`_
  * `/register/google`_
  * `/userinfo/google`_
  * `/request_help/google`_

* `Format`_
  * `Location`_
  * `Notification Preferences`_
  * `Help Request`_


Routes
~~~~~~

List of URL routes and the REST methods supported by each route, including the parameters and returned data.

/register/google
^^^^^^^^^^^^^^^^

**POST** - post registered user info

- *Parameters:*

+--------------------+-------------------+----------------------------------------------------+
| Parameter          | Type              | Info                                               |
+====================+===================+====================================================+
| **REQUIRED**                                                                                |
+--------------------+-------------------+----------------------------------------------------+
| auth               | string            | OAuth 2.0 token received from Google credentials   |
+--------------------+-------------------+----------------------------------------------------+
| email              | string            | Email Address of user                              |
+--------------------+-------------------+----------------------------------------------------+
| registration\_id   | string            | GCM registration id of user                        |
+--------------------+-------------------+----------------------------------------------------+
| **OPTIONAL**                                                                                |
+--------------------+-------------------+----------------------------------------------------+
| name               | string            | Full Name of user                                  |
+--------------------+-------------------+----------------------------------------------------+
| locations          | `Location`_ array | Array of containing latitudes and longitudes       |
+--------------------+-------------------+----------------------------------------------------+

- *Returned Data:* Registered `User Model`_

/userinfo/google
^^^^^^^^^^^^^^^^

**GET** - get user info

- *Parameters:*

+----------------+----------+----------------------------------------------------+
| Parameters     | Type     | Info                                               |
+================+==========+====================================================+
| **REQUIRED**                                                                   |
+----------------+----------+----------------------------------------------------+
| auth           | string   | OAuth 2.0 token received from Google credentials   |
+----------------+----------+----------------------------------------------------+

- *Returned Data:* `User Model`_

**POST** - update/post user info

- *Parameters:*

+---------------------+----------+-------------------------------------------------------------------------------------------+
| Parameters          | Type     | Info                                                                                      |
+=====================+==========+===========================================================================================+
| **REQUIRED**                                                                                                               |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| auth                | string   | OAuth 2.0 token received from Google credentials                                          |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| name                | string   | Username \*                                                                               |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| address1            | string   | Address line 1 \*                                                                         |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| address2            | string   | Address line 2 \*                                                                         |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| city                | string   | City \*                                                                                   |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| state               | string   | State \*                                                                                  |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| zipcode             | string   | Zipcode \*                                                                                |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| phoneNumber         | string   | Phone Number \*                                                                           |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| make                | string   | Make of car \*                                                                            |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| model               | string   | Model of car \*                                                                           |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| licensePlate        | string   | License Plate of car \*                                                                   |
+---------------------+----------+-------------------------------------------------------------------------------------------+
| notificationPrefs   | JSON     | JSON Formatted `Notification Preferences`_ \*\*                                           |
+---------------------+----------+-------------------------------------------------------------------------------------------+

\* **If you don't want to change the value, set it to '' (empty
string)**

\*\* **If you don't want to change the value, set it to {} (empty
JSON)**

- *Returned Data:* Updated `User Model`_

/request_help/google
^^^^^^^^^^^^^^^^^^^^

**GET** - get 20 help requests

- *Parameters:*

+----------------+------------+----------------------------------------------------+
| Parameters     | Type       | Info                                               |
+================+============+====================================================+
| **REQUIRED**                                                                     |
+----------------+------------+----------------------------------------------------+
| auth           | string     | OAuth 2.0 token received from Google credentials   |
+----------------+------------+----------------------------------------------------+
| **OPTIONAL**                                                                     |
+----------------+------------+----------------------------------------------------+
| location       | JSON       | JSON Object containing 'latitude' and 'longitude   |
+----------------+------------+----------------------------------------------------+
| radius         | int        | Within radius, in miles, around the location       |
+----------------+------------+----------------------------------------------------+

- *Returned Data:* `User Model`_

**POST** - post a help

- *Parameters:*

+----------------+----------------+---------------------------------------------------------------------------+
| Parameters     | Type           | Info                                                                      |
+================+================+===========================================================================+
| **REQUIRED**                                                                                                |
+----------------+----------------+---------------------------------------------------------------------------+
| auth           | string         | OAuth 2.0 token received from Google credentials                          |
+----------------+----------------+---------------------------------------------------------------------------+
| helpRequest    | JSON           | JSON encoded `Help Request`_                                              |
+----------------+----------------+---------------------------------------------------------------------------+

- *Returned Data:* `User Model`_

Format
~~~~~~

User Model
^^^^^^^^^^

::

    {
        "userid"        : ...,  // Unique User ID
        "registrationId": ...,  // Google Cloud Messaging device ID

        // Personal Info
        "name"          : ...,
        "email"         : ...,
        "phoneNumber"   : ...,
        "address1"      : ...,
        "address2"      : ...,
        "city"          : ...,
        "state"         : ...,
        "zipcode"       : ...,

        // Other Info
        "make"          : ...,
        "model"         : ...,
        "licensePlate"  : ...,

        "notificationPrefs" : { ... } // Notification Preferences object
    }

Location
^^^^^^^^

::

    {
        "latitude"  : 42.887666,
        "longitude" : -78.878051
    }

Notification Preferences
^^^^^^^^^^^^^^^^^^^^^^^^

::

    {
       "anytime"    : false,
       "startTime"  : ...,   // Notifications start time
       "endTime"    : ...,   // Notification end time

       "facebookNotifications"  : false,
       "twitterNotifications"   : false,
       "emailNotifications"     : false,

       "responseLocations"      : [...],     // List of locations
       "responseTypes:          : [false, true, false, true, false, true, true],
    }


Help Request
^^^^^^^^^^^^

::

    {
        "uid"   : 1460299230554,

        "title"         :"im stuck!",
        "comment"       :"i slid off the road!",
        "licensePlate"  :"eus-5162",
        "make"          :"nissan",
        "model"         :"murano",
        "location"  :
            {
                "latitude"  :42.999598031887004,
                "longitude" :-78.78819834440947
            },
        "urgency"   :5,
        "responseTypes":
            {
                "crash" :false,
                "gas"   :false,
                "person":false,
                "stall" :false,
                "stuck" :true,
                "tire"  :false,
                "other" :false
            }
    }


Error Codes
~~~~~~~~~~~


"""
Functions to access database
"""
from google.appengine.ext import ndb

import models
from helpers import geo


def to_jsonable_data(data):
    """
    Return data is JSON Serializable

    :param data: Data to convert
    :type data: object
    :return: JSON serializable object
    :rtype: object
    """
    if isinstance(data, list):
        return map(to_jsonable_data, data)
    elif isinstance(data, models.HelpRequest):
        dbuid = data.key.urlsafe()
        ret = data.to_dict()
        ret['dbuid'] = dbuid
        return ret
    return data


def get_alerts(lat=None, lon=None, radius=5,to_jsonable=False):
    """
    Get list of all alerts within the given radius

    :param lat: Search location latitude (default = None)
    :type lat: float
    :param lon: Search location lon (default = None)
    :type lon: float
    :param radius: HelpRequest search radius (in miles) around the lat long provided (default = None)
    :type radius: int
    :param to_jsonable: Specify if returned data needs to be JSON serializable
    :type to_jsonable: bool
    :return: List of alerts within radius of coordinates
    :rtype: list
    """
    qry = models.HelpRequest.query()
    if lat and lon:
        # max_lat = helpers.haversine_distance()
        dlat, dlon = geo.get_box_coordinate_diff(lat, lon, radius)
        qry = qry.filter(ndb.AND(
            models.HelpRequest.location.latitude <= lat+dlat,
            models.HelpRequest.location.latitude >= lat - dlat,
            models.HelpRequest.location.longitude <= lon + dlon,
            models.HelpRequest.location.longitude >= lon + dlon,
        ))

    data = {} if not qry else qry.fetch(20)
    if to_jsonable:
        data = to_jsonable_data(data)
    return data


def get_active_alerts(userid='', dbuid='', to_jsonable=False):
    """
    Get active HelpRequests of User affiliated to userid or the HelpRequest with dbuid

    :param userid: User ID of User that created the HelpRequests
    :type userid: str
    :param dbuid: DB UID of the specific HelpRequest
    :type dbuid: str
    :param to_jsonable: Specify if returned data needs to be JSON serializable
    :type to_jsonable: bool
    :return: List of HelpRequest or HelpRequest
    :rtype: object
    """
    ret = None
    if userid:
        usr = query_userid(userid)
        if usr:
            ret = [ndb.Key(urlsafe=url_id).get() for url_id in usr.activeRequests]
    elif dbuid:
        ret = ndb.Key(urlsafe=dbuid).get()
    if to_jsonable:
        ret = to_jsonable_data(ret)
    return ret


def query_userid(userid):
    """Query database for user

    :param userid: Unique userid of user
    :type userid: str
    :return: Queried User
    :rtype: models.ModelUser
    """
    qry = models.ModelUser.get_by_auth_id(userid)
    return qry


def get_key(*args, **kwargs):
    """
    Get NDB Key for specified arglist, or the Key from urlsafe string

    Keyword args are:

    * url_string (str) - urlsafe encoding of the NDB Entity

    :param args: Hierarchy of ndb.Model as specified in NDB Key Reference
    :param kwargs: keyword arglist
    :return: ndb.Key

    .. seealso:: `NDB Keys <https://cloud.google.com/appengine/docs/python/ndb/keyclass>`_
    """
    if 'url_string' in kwargs:
        key = ndb.Key(urlsafe=kwargs['url_string'])
    else:
        key = ndb.Key(*args)
    return key


def get_reg_ids():
    """
    Get GCM Registration IDs of Users

    :return: list
    """
    qry = models.ModelUser.query()
    data = qry.map(lambda usr: usr.registrationId)
    return data

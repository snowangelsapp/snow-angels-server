"""
Module consists of helper functions.
"""

import datetime
import json
from time import mktime
# noinspection PyPackageRequirements,PyPackageRequirements
from google.appengine.ext import ndb
import database


def convert_multi_dict(multi_dict):
    """Convert webob.MultiDict, like request.POST and request.GET to dict

    :param multi_dict: MultiDict to be converted into a Python dict
    :type multi_dict: webob.MultiDict
    :return: dict representation of MultiDict
    :rtype: dict
    """
    return multi_dict.mixed()


# NOTE: When sending JSON data from client side, the data was encoded wrongly.
#       The data became the key mapped to an empty string. To work around this, the data
#       in the request MultiDict, like request.POST, was extracted from the wrongly encoded key.
#       This is a very hacky workaround.
def resolveKey(multi_dict):
    """This converts the encoded JSON key to dict.

    :param multi_dict: request MultiDict that has the bad encoding mentioned
    :type multi_dict: webob.Multidict
    :return: dict
    """
    keys = multi_dict.keys()
    data = {}
    if len(keys) == 1:
        try:
            data = json.loads(keys[0])
        except ValueError:
            return data
    return data


def get_data(multi_dict):
    """
    This returns the Request data, converted to dict, along with auth token, if any

    :param multi_dict: request MultiDict
    :type multi_dict: webob.Multidict
    :return: tuple containing data and auth token in data
    :rtype: (dict, str)
    """
    req_data = convert_multi_dict(multi_dict)
    auth_token = ''
    # if 'auth' in resolveKey(multi_dict):
    #     req_data = resolveKey(multi_dict)
    #     auth_token = req_data['auth']
    # elif 'auth' in req_data:
    #     req_data = convert_multi_dict(multi_dict)
    #     auth_token = req_data['auth']

    if 'auth' in req_data:
        auth_token = req_data['auth']
    else:
        req_data = resolveKey(req_data)
        if 'auth' in req_data:
            auth_token = req_data['auth']
    return req_data, auth_token


def assign_data(data, field, default):
    """
    Determines whether to assign data to a field or not. This will check if the data sent
    has data to replace the field, (if the field has a value in data that is not empty string).

    :param data: dict containing data to update the target field
    :type data: dict
    :param field: key to the field in the data
    :type field: str
    :param default: default value to assign
    :return: the value to assign
    :rtype: object
    """
    if field not in data and not data[field]:
        return default
    else:
        return data[field]


class MyJSONEncoder(json.JSONEncoder):
    """Custom JSON encoder to handle non-JSON serializable classes"""

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(mktime(obj.timetuple()))
        elif isinstance(obj, ndb.Model):
            return json.JSONEncoder.default(self, obj.to_dict())

        return json.JSONEncoder.default(self, obj)










import math


def haversine_distance(lat1, long1, lat2, long2, in_miles=False):
    """
    Haversian distance (as the crow flies) between two GPS coordinates

    :param lat1: First coordinate Latitude
    :type lat1: float
    :param long1: First coordinate Longitude
    :type long1: float
    :param lat2: Second coordinate Latitude
    :type lat2: float
    :param long2: Second coordinate Longitude
    :type long2: float
    :param in_miles: Distance in miles or not (default=False)
    :return: Haversian distance between two coordinates
    :rtype: float

    .. seealso:: `Haversian Distance <https://en.wikipedia.org/wiki/Haversine_formula>`_
    """
    earth_radius = 6373     # in Km
    lat_1 = math.radians(lat1)
    lat_2 = math.radians(lat2)
    diff_lat = math.radians(lat2 - lat1)
    diff_long = math.radians(long2 - long1)

    a = (math.sin(diff_lat/2))**2 + (math.cos(lat_1) * math.cos(lat_2) * (math.sin(diff_long/2))**2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = earth_radius * c    # in Km
    if in_miles:
        return d * 1.6
    return d


def get_box_coordinate_diff(lat, lon, d):
    """
    The maximum differences in Lat and Lon to create square box of side 2d
    with the given position at center

    :param lat: Center position latitude in degrees
    :type lat: float
    :param lon: Center position longitude in degrees
    :type lon: float
    :param d: Distance from center to box edge
    :type d: float
    :return: Tuple containing difference in lat and lon in degrees
    :rtype: (float, float)
    """
    R = 6373
    brng_north = math.radians(0)
    brng_east = math.radians(90)

    lat2 = math.asin(math.sin(lat)*math.cos(d/R) +
                     math.cos(lat)*math.sin(d/R)*math.cos(brng_north))
    lon2 = lon + math.atan2(math.sin(brng_east)*math.sin(d/R)*math.cos(lat),
                            math.cos(d/R)-math.sin(lat)*math.sin(lat))

    return math.degrees(lat2 - lat), math.degrees(lon2 - lon)

# if __name__ == "__main__":
#     # import sys
#     # lat1 = sys.argv[1]
#     # long1 = sys.argv[2]
#     # diff = sys.argv[3]
#     # d = haversine_distance()
#     # d = get_max_coordinate(sys.argv[1], sys.argv[2], sys.argv[3])
#     # d = haversine_distance(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
#
#     lati1 = float(input("Lat1:"))
#     longi1 = float(input("Lon1:"))
#     lati2 = float(input("Lat2:"))
#     longi2 = float(input("Long2:"))
#
#     dist, expected = haversine_distance(lati1, longi1, lati2, longi2)
#     angle = get_max_coordinate(lati1, longi1, dist)
#     print ("Distance: %0.2f \nMax Lat: %f \n" % (dist, angle))
#     print expected
#     print angle
#


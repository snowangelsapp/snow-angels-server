"""Functions to handle Google Cloud Messaging calls"""

import json
import logging
import urllib
import urllib2

import secrets

base_url = "https://gcm-http.googleapis.com/gcm/send"
api_key = secrets.gcm['api_key']
content_type = 'application/json'


def check_notify(data):
    """
    Check if the GCM message was successfully sent.

    This returns a dict containing the fields 'success', 'replacements', 'deletes'.

    * success (bool) - True if messages were successfully sent

    * replacements (list) - list of tuples, contains the registration id to be replaced and the id to replace with

    * deletes (list) - list of registration ids to be deleted

    :param data: Data returned from GCM
    :return: dict

    .. seealso:: `Actions to perform for each error code <https://developers.google.com/cloud-messaging/http-server-ref#error-codes>`_
    """
    ret_data = {
        "success": False,
        "replacements": [],
        "deletes": []

    }
    if 'failure' in data and data['failure'] > 0:
        index = 0
        for entry in data['results']:
            if 'message_id' in entry and 'registration_id' in entry:
                ret_data['replacements'].append(tuple([index, entry['registration_id']]))
            elif 'error' in entry:
                logging.error('GCM: %s\n' % entry['error'])
                pass
            index += 1
    else:
        ret_data['success'] = True
    return ret_data


def notify_ids(registration_ids, notification_data=None, data=None):
    """
    Send specified GCM message to specified GCM Registration IDs

    :param registration_ids: List of registration ids
    :type registration_ids: list
    :param notification_data: dict containing 'notification' content
    :type notification_data: dict
    :param data: dict containing 'data' content
    :type data: dict
    :return: Specifies successful or not
    :rtype: bool

    .. seealso:: `HTTP GCM Payload <https://developers.google.com/cloud-messaging/http-server-ref#downstream-http-messages-json>`_
    """

    params = json.dumps(
        {
            "notification": notification_data,
            "data": data,
            "registration_ids": registration_ids
        })
    headers = {"Authorization": urllib.urlencode({"key": api_key}),
               "Content-Type": content_type,
               "Content-Length": len(params)}
    req = urllib2.Request(base_url, data=params, headers=headers)
    try:
        res = urllib2.urlopen(req)
        res_data = json.loads(res.read())
        logging.info(json.dumps(res_data))
        check = check_notify(res_data)
        if check['success']:
            return True
        # else:
        #     logging.info(json.dumps(check))
        #     return False
    except urllib2.HTTPError as e1:
        logging.error("%s: %s" % (e1.code, e1.read()))
        pass
    except urllib2.URLError as e2:
        logging.error(e2.reason)
        pass
    except Exception as e3:
        logging.error(e3)
        pass

    # print ("END")
    return False

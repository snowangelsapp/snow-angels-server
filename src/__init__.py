"""
The main entry point of the server script
"""

import logging

# noinspection PyPackageRequirements,PyPackageRequirements
import webapp2

import secrets
from handlers import google, database, pages, help_request, respond

config = {
    'webapp2_extras.auth': {
        'user_model': 'models.ModelUser',
        'user_attributes': ['userid']
    },
    'google_client_id': secrets.google['client_id']
}

routes = [
    webapp2.Route(r'/', pages.MainPage, methods=['GET']),
    webapp2.Route(r'/register/google', google.GoogleSignupHandler,
                  methods=['POST', 'GET']),
    webapp2.Route(r'/userinfo/google', database.UserInfo,
                  methods=['GET', 'POST']),
    webapp2.Route(r'/request_help/google', help_request.AlertsHandler,
                  methods=['GET', 'POST']),
    webapp2.Route(r'/respond/google', respond.RespondHandler,
                  methods=['POST'])
]

app = webapp2.WSGIApplication(routes, debug=True, config=config)

logging.getLogger().setLevel(logging.DEBUG)

from oauth2client import client
import json
import urllib


def authenticateGoogle(token, client_id):
    """Authenticate Google JWT auth token using oauth2client module

    :deprecated: The oauth2client module doesn't seem to be working; throws a BadRequest exception

    :param token: auth token
    :type token: str
    :param client_id: list of valid client ids
    :type client_id: list
    :return: Tuple (bool, dict) containing bool to check if auth token was valid, and dict containing decoded JWT
    :rtype: (bool, dict)

    """
    idinfo = client.verify_id_token(token, client_id)
    if idinfo['iss'] not in ['accounts.google.com',
                             'http://accounts.google.com']:
        return False, idinfo
    return True, idinfo


def authenticate_google(token, client_ids):
    """
    Authenticate Google JWT auth token using GET request to Google OAuth API

    :param token: Google JWT auth token
    :type token: str
    :param client_ids: list of valid client ids
    :type client_ids: list
    :return: Tuple (bool, dict) containing bool to check if auth token was valid, and dict containing decoded JWT
    :rtype: (bool, dict)
    """

    urlpath = "https://www.googleapis.com/oauth2/v3/tokeninfo?access_token="

    res = urllib.urlopen(urlpath + token)
    idinfo = json.loads(res.read())
    if 'aud' in idinfo and idinfo['aud'] in client_ids:
        return True, idinfo
    return False, idinfo

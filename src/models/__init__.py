import time

import webapp2_extras.appengine.auth.models
# noinspection PyPackageRequirements,PyPackageRequirements
from google.appengine.ext import ndb


class Location(ndb.Model):
    """NDB Model for Location"""

    latitude = ndb.FloatProperty()
    """Latitude"""
    longitude = ndb.FloatProperty()
    """Longitude"""
    radius = ndb.IntegerProperty()
    """Radius"""


class ResponseTypes(ndb.Model):
    """NDB Model for Response Types

    Consists of various situations with bool values
    """
    crash = ndb.BooleanProperty()
    """Crash (bool)"""
    gas = ndb.BooleanProperty()
    """Gas (bool)"""
    person = ndb.BooleanProperty()
    """Person (bool)"""
    stall = ndb.BooleanProperty()
    """Stall (bool)"""
    stuck = ndb.BooleanProperty()
    """Stuck (bool)"""
    tire = ndb.BooleanProperty()
    """Tire (bool)"""
    other = ndb.BooleanProperty()
    """Other (bool)"""


class NotificationPref(ndb.Model):
    """NDB Model for Notification Preferences"""
    anyTime = ndb.BooleanProperty()
    """Anytime notification (bool)"""
    startTime = ndb.TimeProperty()
    """Start time (time)"""
    endTime = ndb.TimeProperty()
    """End time (time)"""
    facebookNotifications = ndb.BooleanProperty()
    """Facebook Notification (bool)"""
    twitterNotifications = ndb.BooleanProperty()
    """Twitter Notification (bool)"""
    emailNotifications = ndb.BooleanProperty()
    """Email Notification (bool)"""
    responseLocations = ndb.StructuredProperty(Location, repeated=True)
    """Array of Locations"""
    responseTypes = ndb.BooleanProperty(repeated=True)
    """Response types, array of bool"""


class ModelUser(webapp2_extras.appengine.auth.models.User):
    """NDB Model for the User"""

    registrationId = ndb.StringProperty()
    """GCM Registration ID (str)"""
    name = ndb.StringProperty()
    """Name (str)"""
    email = ndb.StringProperty()
    """Email (str)"""
    address1 = ndb.StringProperty()
    """Address Line 1 (str)"""
    address2 = ndb.StringProperty()
    """Address Line 2 (str)"""
    city = ndb.StringProperty()
    """City (str)"""
    state = ndb.StringProperty()
    """State (str)"""
    zipcode = ndb.StringProperty()
    """Zipcode (str)"""
    phoneNumber = ndb.StringProperty()
    """Phone Number (str)"""
    make = ndb.StringProperty()
    """Car Make (str)"""
    model = ndb.StringProperty()
    """Car Model (str)"""
    licensePlate = ndb.StringProperty()
    """License Plate (str)"""
    notificationPrefs = ndb.StructuredProperty(NotificationPref)
    """Notification preferences"""
    activeRequests = ndb.StringProperty(repeated=True)
    """dbuid of Active Help Requests"""
    pastRequests = ndb.StringProperty(repeated=True)
    """dbuid of past requests"""
    responsedRequests = ndb.StringProperty(repeated=True)
    """dbuid of past responses"""

    @classmethod
    def get_by_auth_token(cls, user_id, token, subject='auth'):
        """Returns user object based on user_id and token"""

        token_key = cls.token_model.get_key(user_id, subject, token)
        user_key = ndb.Key(cls, user_id)
        valid_token, user = ndb.get_multi([token_key, user_key])
        if valid_token and user:
            timestamp = int(time.mktime(valid_token.created.timetouple()))
            return user, timestamp
        return None, None


class HelpRequest(ndb.Model):
    """NDB Model for Alerts"""
    title = ndb.StringProperty()
    """Title (str)"""
    comment = ndb.TextProperty()
    """Comment (str)"""
    licensePlate = ndb.StringProperty()
    """License Plate (str)"""
    make = ndb.StringProperty()
    """Car Make (str)"""
    model = ndb.StringProperty()
    """Car Model (str)"""
    location = ndb.StructuredProperty(Location)
    """Location"""
    urgency = ndb.FloatProperty()
    """Urgency (int)"""
    uid = ndb.FloatProperty()
    """Unique ID"""
    responseTypes = ndb.StructuredProperty(ResponseTypes)
    """Response Types"""
    userid = ndb.StringProperty()
    """User ID of Creator"""
    created = ndb.DateTimeProperty(auto_now=True)
    """Time the Request is created"""
    responded = ndb.BooleanProperty()
    """Boolean to check if request has been responded to or not"""

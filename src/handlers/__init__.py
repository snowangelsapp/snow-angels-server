"""
Module containing all the the request handlers for the API.

All request handlers inherit from
`webapp2.RequestHandler <https://webapp-improved.appspot.com/api/webapp2.html#webapp2.RequestHandler>`_.
"""

import json
import logging

# noinspection PyPackageRequirements,PyPackageRequirements
import webapp2
from webapp2_extras import auth

import helpers
from authenticate import auth_google


class BaseHandler(webapp2.RequestHandler):
    """Parent class for all RequestHandlers"""

    @webapp2.cached_property
    def auth(self):
        """Shortcut to access the auth instance as a property."""
        return auth.get_auth()

    @webapp2.cached_property
    def user_model(self):
        """Returns the implementation of the user model.

        Consistent with config['webapp2_extras.auth']['user_model'], if set.
        """
        return self.auth.store.user_model

    def register(self, userid):
        """
        Register User

        :param userid: Unique user id of the User. Generate from methods like authenticateGoogle and authenticate_google
        :type userid: str
        :return: tuple consisting of whether the registering was successful and the User entry; (bool, data)
        :rtype: tuple
        """

        req_data = helpers.resolveKey(self.request.POST)
        try:
            email = req_data['email']
        except KeyError:
            self.response.status = '401 Email not provided'
            # self.response.body = 'Email not provided\n'
            logging.error('Email not provided\n')
            return
        try:
            register_id = req_data['registrationId']
        except KeyError:
            self.response.status = '401 GCM Registration ID not provided'
            logging.error(self.response.status)
            return

        try:
            name = req_data['name']
        except KeyError:
            name = ''

        user_data = self.user_model.create_user(userid,
                                                unique_ptys,
                                                userid=userid,
                                                email=email,
                                                name=name,
                                                registrationId=register_id,
                                                new=True)
        logging.info(user_data)
        return user_data

    def getUserID(self, req_data):
        """
        Get unique user id from request data

        :param req_data: Dict of parameters sent in request, e.g. request.POST, request.GET
        :type req_data: dict
        :return: unique userid
        :rtype: str
        """
        path = self.request.path
        if 'auth' not in req_data:
            self.response.status = '401 No auth token provided'
            # self.response.write('No auth token provided')
            logging.error('No auth token provided')
            logging.debug('Data:' + json.dumps(req_data))
            return ''
        auth_token = req_data['auth']
        if 'google' in path:
            client_ids = self.app.config.get('google_client_id')
            # check, info = self.authenticate_google(auth_token, client_ids)
            check, info = auth_google.authenticate_google(auth_token, client_ids)
            if not check:
                self.response.status = '401 Invalid auth token'
                # self.response.write('Invalid auth token')
                logging.error('Invalid auth token: %s' % auth_token)
                return None
            auth_id = '%s:%s' % ('google', info['sub'])
            return auth_id
        return ''


# baseHandler = BaseHandler
# googleHandler = google.GoogleSignupHandler
# userInfoHandler = database.UserInfo
# mainPageHandler = pages.MainPage

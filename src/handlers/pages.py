from handlers import BaseHandler


class MainPage(BaseHandler):
    """Handles Main page of application website"""
    def get(self):
        # self.response.write('Hello!')
        self.redirect('/docs/')
        return

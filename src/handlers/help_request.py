import json
import logging

import helpers
import models
from handlers import BaseHandler
from helpers import database as db
from helpers import gcm


class AlertsHandler(BaseHandler):
    """Handles help requests

    path - /request_help/google
    """

    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        req_data, auth = helpers.get_data(self.request.GET)
        if not auth:
            self.response.status = '401 No Auth Token Provided'
            self.response.write(self.response.status)
            return
        auth_id = self.getUserID(req_data)
        qry = db.get_alerts(to_jsonable=True)
        if qry:
            self.response.write(json.dumps(qry,
                                           cls=helpers.MyJSONEncoder))

        logging.info(self.response.body)
        return

    def post(self):
        self.response.headers['Content-Type'] = 'application/json'
        req_data = helpers.get_data(self.request.POST)[0]
        userid = self.getUserID(req_data)
        if not userid:
            self.response.status = '401 Unregistered User or Invalid Auth '
            self.response.write(self.response.status)
            return
        parent_user = db.query_userid(userid).key.id()
        key = None
        dbuid = ''
        if 'delete' in req_data and req_data['delete']:
            delkey = db.get_key(url_string=req_data['delete'])
            delkey.delete()
            return
        if 'updated' not in req_data:
            self.response.status = '401 Invalid Parameters'
            logging.error(self.response.status)
            return
        elif not req_data['updated']:
            key = db.get_key(models.ModelUser, parent_user, models.HelpRequest, str(req_data['helpRequest']['uid']))
            alert = models.HelpRequest()
            alert.key = key
            dbuid = alert.key.urlsafe()
        else:
            try:
                key = db.get_key(url_string=req_data['helpRequest']['dbuid'])
            except KeyError:
                self.response.status = '401 dbuid not provided'
                logging.error(self.response.status)
                return
            alert = key.get()
            dbuid = alert.key.urlsafe()

        input_data = req_data['helpRequest']
        if 'dbuid' in input_data:
            input_data.pop('dbuid', None)
        alert.populate(**input_data)
        alert.responded = False
        # alert.created = datetime.datetime.now()
        creator = db.query_userid(userid)
        creator.activeRequests.append(dbuid)
        validate = alert.put()

        entity = validate.get()
        data = {}
        if entity:
            data['dbuid'] = entity.key.urlsafe()
            data['error'] = None

        self.response.write(json.dumps(data))
        logging.info("Data: %s" % json.dumps(data))
        notification = {
            'title': 'New Help Request',
            'body': 'Somebody needs help around you',
            "click_action": "OPEN_MAIN_ACTIVITY"
        }
        msg_data = {
            'message': 'New Help Request'
        }
        gcm.notify_ids(db.get_reg_ids(), notification, msg_data)
        return

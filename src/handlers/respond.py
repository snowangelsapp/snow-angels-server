import helpers
from handlers import BaseHandler

from helpers import database as db


class RespondHandler(BaseHandler):
    """Handles responding to help requests

    path - /respond/google
    """

    def get(self):
        pass

    def post(self):
        self.response.headers['Content-Type'] = 'application/json'
        req_data = helpers.get_data(self.request.POST)[0]
        userid = self.getUserID(req_data)
        if not userid:
            self.response.status = '401 Unregistered User or Invalid Auth '
            self.response.write(self.response.status)
            return
        dbuid = helpers.assign_data(req_data, 'dbuid', '')
        if not dbuid:
            self.response.status = '401 dbuid Not Provided'
            self.response.write(self.response.status)
            return
        alert = db.get_key(url_string=dbuid).get()
        alert.responded = True
        alert.put()

        # TODO Tell requester that its been responded
        pass


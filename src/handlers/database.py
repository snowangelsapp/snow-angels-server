import json
import logging

import helpers
import models
from handlers import BaseHandler
from helpers import database as db


class UserInfo(BaseHandler):
    """Handles userinfo request

    path - /userinfo/google
    """

    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        req_data, auth = helpers.get_data(self.request.GET)
        auth_id = self.getUserID(req_data)
        qry = db.query_userid(auth_id)
        if qry:
            # qry.new = False
            qry = qry.to_dict()
            qry.pop('password', None)
            qry.pop('auth_ids', None)
            active_alerts = db.get_active_alerts(userid=auth_id, to_jsonable=True)
            qry['requests'] = active_alerts
            self.response.write(json.dumps(qry,
                                           cls=helpers.MyJSONEncoder))
        else:
            self.response.write(json.dumps({}))
        return

    def post(self):
        self.response.headers['Content-Type'] = 'application/json'
        req_data, auth = helpers.get_data(self.request.POST)
        logging.info(req_data)
        auth_id = self.getUserID(req_data)
        qry = db.query_userid(auth_id)
        if qry:
            qry.name = helpers.assign_data(req_data, 'name', qry.name)
            qry.address1 = helpers.assign_data(
                req_data, 'address1', qry.address1)
            qry.address2 = helpers.assign_data(
                req_data, 'address2', qry.address2)
            qry.city = helpers.assign_data(req_data, 'city', qry.city)
            qry.state = helpers.assign_data(req_data, 'state', qry.state)
            qry.zipcode = helpers.assign_data(req_data, 'zipcode', qry.zipcode)
            qry.phoneNumber = helpers.assign_data(
                req_data, 'phoneNumber', qry.phoneNumber)
            qry.make = helpers.assign_data(req_data, 'make', qry.make)
            qry.model = helpers.assign_data(req_data, 'model', qry.model)
            qry.licensePlate = helpers.assign_data(
                req_data, 'licensePlate', qry.licensePlate)

            qry.new = False

            notif_pref = models.NotificationPref(
                **req_data['notificationPrefs'])
            qry.notificationPrefs = notif_pref
            qry.put()
            qry = qry.to_dict()
            qry.pop('password', None)
            qry.pop('auth_ids', None)
            self.response.write(json.dumps(qry,
                                           cls=helpers.MyJSONEncoder))
        else:
            self.response.status = '401 Unregistered User'
            self.response.body = ' Unregistered User'
        logging.info(self.response.body)
        return

import json
import logging

import helpers
from authenticate import auth_google
from handlers import BaseHandler
from helpers import database as db


class GoogleSignupHandler(BaseHandler):
    """Handles Google signup requests

    path - /register/google
    """

    def post(self):
        self.response.headers['Content-Type'] = 'application/json'

        req_data, auth_token = helpers.get_data(self.request.POST)

        # logging.info("AUTH:      " + auth_token)
        #
        # if auth_token == '':
        #     self.response.status = '401 Auth Token not provided'
        #     self.response.body = 'Auth Token not provided\n'
        #     return
        #
        # client_id = self.app.config.get('google_client_id')
        #
        # # logging.info("CLIENT_ID: " + json.dumps(client_id))
        # # verified, idinfo = self.authenticate_google(auth_token, client_id)
        # verified, idinfo = auth_google.authenticate_google(auth_token, client_id)
        # if not verified:
        #     self.response.status = '401 Invalid Auth Token'
        #     self.response.body = 'Invalid Auth Token\n'
        #     logging.info("Invalid Auth Token")
        #     # logging.info(idinfo)
        #     return
        # userid = "%s:%s" % ('google', idinfo['sub'])

        userid = self.getUserID(req_data)
        logging.info("UserID: %s" % userid)
        if not userid:
            return
        qry = db.query_userid(userid)
        logging.info("Query: %s" % qry)
        valid = False
        user_data = None
        new = False
        if not qry:
            valid, user_data = self.register(userid)
            new = True
        else:
            user_data = qry
            valid = True

        if valid:
            user_data.new = new
            user_data.registrationId = helpers.assign_data(req_data, 'registrationId',
                                                           user_data.registrationId)
        if not valid:
            self.response.status = '401 Possible Duplicate Data'
            logging.error(self.response.status)
            logging.debug('Data:' + json.dumps(req_data))
            return
        user_data.put()
        self.response.status = '200 OK'
        user_data = user_data.to_dict()
        user_data.pop('password', None)
        user_data.pop('auth_ids', None)
        self.response.body = json.dumps(user_data,
                                        cls=helpers.MyJSONEncoder)
        logging.info(self.response.body)
        return

    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        req_data, auth_token = helpers.get_data(self.request.GET)
        data = auth_google.authenticate_google(auth_token,
                                               self.app.config.get('google_client_id'))
        self.response.body = data[1]
        return
